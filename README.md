# Terminal Prompt
An amazingly useful and beautiful terminal prompt for linux/mac

## How to use
1) Download the repository and extract it to a folder.
2) Copy and paste the contents of append_to_bashrc file to your .bashrc file.
3) Your .bashrc file is present in your home folder.

## Screenshots

![Screenshot of terminal prompt](/img.png)

It also shows git related informations like "git branch" etc. when you are in a git repository.

![Screenshot of terminal prompt](/img2.png)


# Author
Shubham Prakash

(shubham.prakash2308@gmail.com)

LinkedIn: https://www.linkedin.com/in/ishubhamprakash/


